package com.mygdx.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.mygdx.game.BalloonBuster1;

public class DesktopLauncher {
	public static void main (String[] arg) {
		/*LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new BalloonBuster1(), config);*/
		BalloonGame myProgram = new BalloonGame();
		LwjglApplication launcher = new LwjglApplication(myProgram);
	}
}
